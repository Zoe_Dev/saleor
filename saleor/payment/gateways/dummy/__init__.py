import uuid
import requests
import json
import array as arr
from django_countries import countries
from ... import ChargeStatus, TransactionKind
from ...interface import GatewayConfig, GatewayResponse, PaymentData

def send_api(payment_information: PaymentData, config: GatewayConfig):

    print("config parameters", config.connection_params)
    print("We are about to ignite", payment_information)
    data: Dict[str, str] = {}
    for key in config.connection_params:
        merchant_id = config.connection_params[key]
    
    if payment_information:
        billing_info = payment_information.billing
        shipping_info = payment_information.shipping
        amount_info = payment_information.amount
        currency = payment_information.currency
        customer_email = payment_information.customer_email
        country = dict(countries)[shipping_info.country]
        data = {
            "first_name": billing_info.first_name,
            "last_name": billing_info.last_name,
            "shipping_address": payment_information.shipping,
            "delivery_country": country,
            "amount_info": amount_info,
            "sub_total": payment_information.sub_total,
            "shipping_cost": payment_information.shipping_cost,
            "currency": currency,
            "customer_email": customer_email,
            "merchant_id": merchant_id,
        }
    response = requests.post('https://thanos.free.beeceptor.com',data = data)
    print("Be prepared mazee for anything", response.status_code)
    return response.status_code

def dummy_success():
    return True


def get_client_token(**_):
    return str(uuid.uuid4())


def authorize(
    payment_information: PaymentData, config: GatewayConfig
) -> GatewayResponse:
    #success = dummy_success()
    success = False
    payment_status_code = send_api(payment_information,config)
    if payment_status_code == 200:
        success = True
    error = None
    if not success:
        error = "Unable to authorize transaction"
    return GatewayResponse(
        is_success=success,
        action_required=False,
        kind=TransactionKind.AUTH,
        amount=payment_information.amount,
        currency=payment_information.currency,
        transaction_id=payment_information.token,
        error=error,
    )


def void(payment_information: PaymentData, config: GatewayConfig) -> GatewayResponse:
    error = None
    #success = dummy_success()
    success = False
    payment_status_code = send_api(payment_information,config)
    if payment_status_code == 200:
        success = True
    if not success:
        error = "Unable to void the transaction."
    return GatewayResponse(
        is_success=success,
        action_required=False,
        kind=TransactionKind.VOID,
        amount=payment_information.amount,
        currency=payment_information.currency,
        transaction_id=payment_information.token,
        error=error,
    )


def capture(payment_information: PaymentData, config: GatewayConfig) -> GatewayResponse:
    """Perform capture transaction."""
    error = None
    #success = dummy_success()
    success = False
    payment_status_code = send_api(payment_information,config)
    if payment_status_code == 200:
        print("for now im not sure")
        success = True
    if not success:
        error = "Unable to process capture"

    return GatewayResponse(
        is_success=success,
        action_required=False,
        kind=TransactionKind.CAPTURE,
        amount=payment_information.amount,
        currency=payment_information.currency,
        transaction_id=payment_information.token,
        error=error,
    )


def confirm(payment_information: PaymentData, config: GatewayConfig) -> GatewayResponse:
    """Perform confirm transaction."""
    error = None
    #success = dummy_success()
    success = False
    payment_status_code = send_api(payment_information,config)
    if payment_status_code == 200:
        success = True
    if not success:
        error = "Unable to process capture"

    return GatewayResponse(
        is_success=success,
        action_required=False,
        kind=TransactionKind.CAPTURE,
        amount=payment_information.amount,
        currency=payment_information.currency,
        transaction_id=payment_information.token,
        error=error,
    )


def refund(payment_information: PaymentData, config: GatewayConfig) -> GatewayResponse:
    error = None
    #success = dummy_success()
    success = False
    payment_status_code = send_api(payment_information,config)
    if payment_status_code == 200:
        success = True
    if not success:
        error = "Unable to process refund"
    return GatewayResponse(
        is_success=success,
        action_required=False,
        kind=TransactionKind.REFUND,
        amount=payment_information.amount,
        currency=payment_information.currency,
        transaction_id=payment_information.token,
        error=error,
    )


def process_payment(
    payment_information: PaymentData, config: GatewayConfig
) -> GatewayResponse:
    """Process the payment."""
    token = payment_information.token
    send_api(payment_information,config)
    # Process payment normally if payment token is valid
    if token not in dict(ChargeStatus.CHOICES):
        return capture(payment_information, config)

    # Process payment by charge status which is selected in the payment form
    # Note that is for testing by dummy gateway only
    charge_status = token
    authorize_response = authorize(payment_information, config)
    print("Authorize response",authorize_response) 
    if charge_status == ChargeStatus.NOT_CHARGED:
        return authorize_response

    if not config.auto_capture:
        return authorize_response

    capture_response = capture(payment_information, config)
    if charge_status == ChargeStatus.FULLY_REFUNDED:
        return refund(payment_information, config)
    return capture_response
